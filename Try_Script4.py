import numpy as np
import numpy.random as npr
import scipy as scp
import scipy.interpolate as scpi
import scipy.sparse as scps
from scipy.sparse import lil_matrix
from scipy.sparse import csr_matrix
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import DiscretizationMatrix
import Reshape
import LinearProblemVector
import PlotFunctions
import MeshMethods
import FourierGeneration

xAxisPointsNumber = 250
yAxisPointsNumber = 250
xLimits = [-1, 1]
yLimits = [-1, 1]
mu = 0.0
sigma2 = 0.1
regularityCoef = 2.0
thresholdPerPoint = 1e-10
maxIterationsNumber = 10000
address = "./savedMesh/"
filename = "mesh_7Mai2019"
verboseFlag = True
iterationModulo = 50
plotFlag = True
curvesNumber = 150
meshTitle = ""
xAxisTitle = "x"
yAxisTitle = "y"
zAxisTitle = "z"
joinedDescription = ""
Zs, Zh = FourierGeneration.gen2DMesh(   xAxisPointsNumber,
                                        yAxisPointsNumber,
                                        xLimits, 
                                        yLimits,
                                        mu,
                                        sigma2,
                                        regularityCoef,
                                        thresholdPerPoint,
                                        maxIterationsNumber,
                                        address,
                                        filename,
                                        verboseFlag = verboseFlag,
                                        iterationModulo = iterationModulo,
                                        plotFlag = plotFlag,
                                        curvesNumber = curvesNumber,
                                        meshTitle = meshTitle,
                                        xAxisTitle = xAxisTitle,
                                        yAxisTitle = yAxisTitle,
                                        zAxisTitle = zAxisTitle,
                                        joinedDescription = joinedDescription)

print("Moyenne empirique : ", np.mean(Zh))
print("Variance empirique : ", np.var(Zh))