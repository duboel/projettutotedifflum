import numpy as np
import numpy.random as npr
import scipy as scp
import scipy.interpolate as scpi
import scipy.sparse as scps
from scipy.sparse import lil_matrix
from scipy.sparse import csr_matrix
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os
import re

def saveMeshToFile(address, fileName, xLimits, yLimits, mesh, title = "", xTitle = "x", yTitle = "y", zTitle = "z", joinedDescription = "") :

    if os.path.isfile(address + "/" + fileName + ".mesh") :
        counter = 2
        while os.path.isfile(address + "/" + fileName + "_" + str(counter) + ".mesh") :
            counter += 1

        stream = open(address + "/" + fileName + "_" + str(counter) + ".mesh", "w+")

    else :

        stream = open(address + "/" + fileName + ".mesh", "w+")

    yAxisPointsNumber, xAxisPointsNumber = mesh.shape
    
    ###############################
    ###### Writing headlines ######
    ###############################
    buffer = "xAxisPointsNumber : "
    buffer += str(xAxisPointsNumber)
    buffer += "\n"
    stream.write(buffer)

    buffer = "yAxisPointsNumber : "
    buffer += str(yAxisPointsNumber)
    buffer += "\n"
    stream.write(buffer)

    buffer = "xLimits : "
    buffer += str(xLimits[0])
    buffer += " "
    buffer += str(xLimits[1])
    buffer += "\n"
    stream.write(buffer)

    buffer = "yLimits : "
    buffer += str(yLimits[0])
    buffer += " "
    buffer += str(yLimits[1])
    buffer += "\n"
    stream.write(buffer)
    ###############################
    ###############################
    
    ##########################
    ###### Writing mesh ######
    ##########################
    buffer = "Mesh values : \n"
    stream.write(buffer)

    for yIndex in range(yAxisPointsNumber) : 
        for xIndex in range(xAxisPointsNumber) :
            buffer = str(mesh[yAxisPointsNumber - 1 - yIndex, xIndex])
            buffer += "\n"
            stream.write(buffer)
    ##########################
    ##########################

    #######################################
    ###### Writing Others Parameters ######
    #######################################
    buffer = "Title : "
    buffer += title
    buffer += "\n"
    stream.write(buffer)

    buffer = "xTitle : "
    buffer += xTitle
    buffer += "\n"
    stream.write(buffer)

    buffer = "yTitle : "
    buffer += yTitle
    buffer += "\n"
    stream.write(buffer)

    buffer = "zTitle : "
    buffer += zTitle
    buffer += "\n"
    stream.write(buffer)

    buffer = "Description : "
    buffer += joinedDescription
    buffer += "\n"
    stream.write(buffer)
    #######################################
    #######################################

    stream.close()

def readMeshFromFile(address, fileName) :

    if os.path.isfile(address + "/" + fileName + ".mesh") : 

        stream = open(address + "/" + fileName + ".mesh", "r")
        regEx = re.compile(r"((?<!\d)(-)?(\d)+(\.\d+)?(e(-)?\d+)?)")
        
        #### get xAxisPointsNumber
        buffer = stream.readline()
        match = regEx.findall(buffer)
        xAxisPointsNumber = int(match[0][0])

        #### get yAxisPointsNumber
        buffer = stream.readline()
        match = regEx.findall(buffer)
        yAxisPointsNumber = int(match[0][0])

        #### get xLimits
        buffer = stream.readline()
        match = regEx.findall(buffer)
        xLimits = [float(match[0][0]), float(match[1][0])]

        #### get yLimits
        buffer = stream.readline()
        match = regEx.findall(buffer)
        yLimits = [float(match[0][0]), float(match[1][0])]

        #### get mesh
        buffer = stream.readline()
        mesh = np.zeros([yAxisPointsNumber, xAxisPointsNumber])
        for yIndex in range(1, yAxisPointsNumber + 1) :
            for xIndex in range(xAxisPointsNumber) :

                buffer = stream.readline()
                match = regEx.findall(buffer)
                mesh[yAxisPointsNumber - yIndex, xIndex] = float(match[0][0])

        #### get title
        regEx = re.compile(r"(?<=(^Title : ))(\w)*")
        buffer = stream.readline()
        match = regEx.search(buffer)
        title = match[0]

        #### get xTitle
        regEx = re.compile(r"(?<=(^xTitle : ))(\w)*")
        buffer = stream.readline()
        match = regEx.search(buffer)
        xTitle = match[0]

        #### get yTitle
        regEx = re.compile(r"(?<=(^yTitle : ))(\w)*")
        buffer = stream.readline()
        match = regEx.search(buffer)
        yTitle = match[0]

        #### get zTitle
        regEx = re.compile(r"(?<=(^zTitle : ))(\w)*")
        buffer = stream.readline()
        match = regEx.search(buffer)
        zTitle = match[0]

        #### get description
        regEx = re.compile(r"(?<=(^Description : ))(\w)*")
        buffer = stream.readline()
        match = regEx.search(buffer)
        description = match[0]

        return (    xAxisPointsNumber,
                    yAxisPointsNumber,
                    xLimits,
                    yLimits,
                    mesh,
                    title,
                    xTitle,
                    yTitle,
                    zTitle,
                    description)

    else : 

        raise ValueError