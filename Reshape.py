import numpy as np
import numpy.random as npr
import scipy as scp
import scipy.interpolate as scpi
import scipy.sparse as scps
from scipy.sparse import lil_matrix
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def getMesh2D(topSide, bottomSide, leftSide, rightSide, interiorDomain) :

    xAxisPointsNumber = len(topSide)
    yAxisPointsNumber = len(leftSide) + 2
    mesh = np.zeros([yAxisPointsNumber, xAxisPointsNumber])
    mesh[0,:] = topSide
    mesh[-1,:] = bottomSide
    mesh[1:-1, 0] = leftSide
    mesh[1:-1, -1] = rightSide
    
    getIndex = lambda i, j : (i - 1) + (xAxisPointsNumber - 2)*(j - 1)
    
    for i in range(1, xAxisPointsNumber - 1) :
        for j in range(1, yAxisPointsNumber - 1) :
            mesh[j,i] = interiorDomain[getIndex(i,j)]

    return mesh
