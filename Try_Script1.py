import numpy as np
import numpy.random as npr
import scipy as scp
import scipy.interpolate as scpi
import scipy.sparse as scps
from scipy.sparse import lil_matrix
from scipy.sparse import csr_matrix
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import DiscretizationMatrix
import Reshape
import LinearProblemVector
import PlotFunctions
import MeshMethods

#################################
######## Grid parameters ########
#################################
xAxisPointsNumber = 2500
yAxisPointsNumber = 2500
xLimits = [-5.0, 5.0]
yLimits = [-5.0, 5.0]
xDelta = (xLimits[1] - xLimits[0])/float(xAxisPointsNumber - 1)
yDelta = (yLimits[1] - yLimits[0])/float(yAxisPointsNumber - 1)
#################################
#################################

##################################
######## Sides conditions ########
##################################
topSide = np.zeros(xAxisPointsNumber)
bottomSide = np.zeros(xAxisPointsNumber)
leftSide = np.zeros(yAxisPointsNumber - 2)
rightSide = np.zeros(yAxisPointsNumber - 2)
##################################
##################################

########################################
######## functions for gradient ########
########################################
xSigma2 = 0.02
xDifferentialFunction = lambda x,y : npr.normal(0.0, xSigma2*xDelta)
ySigma2 = 0.002
yDifferentialFunction = lambda x,y : npr.normal(0.0, ySigma2*yDelta)
########################################
########################################

print("Computing discretization matrix... IN PROGRESS")
discretizationMatrix = csr_matrix(DiscretizationMatrix.getDiscretizationMatrix2D(xLimits, yLimits, xAxisPointsNumber, yAxisPointsNumber))

print("Computing discretization matrix... OK")
print("Computing second-membered vector... IN PROGRESS")
linearProblemVector = LinearProblemVector.getLinearProblemVector2D(xLimits, yLimits, topSide, bottomSide, leftSide, rightSide, xDifferentialFunction, yDifferentialFunction)

print("Computing second-membered vector... OK")
print("Computing conjugate-gradient algorithm... IN PROGRESS")
threshold = xAxisPointsNumber*yAxisPointsNumber*1e-6
meshVector = scps.linalg.cg(discretizationMatrix.transpose().dot(discretizationMatrix), discretizationMatrix.transpose().dot(linearProblemVector), tol=threshold)

print("Computing conjugate-gradient algorithm... OK")
print("Reshaping... IN PROGRESS")
mesh = Reshape.getMesh2D(topSide, bottomSide, leftSide, rightSide, meshVector[0])

print("Reshaping... OK")
curvesNumber = 100
title = "Mesh - xSigma2 = " + str(np.round(xSigma2*xDelta, 5)) + " - ySigma2 = " + str(np.round(ySigma2*yDelta, 7))
zTitle = "Surface height"
PlotFunctions.plotMesh2D(xLimits, yLimits, mesh, curvesNumber, title=title, zTitle=zTitle)
plt.show()

# address = "/media/nicolas/Riemann/DocumentsCourants/etudes/INSA/Quatrieme_Annee_GMM_MMN/Annee_2/Semestre 2/ProjetTutore/Work_16Avril2019"
address = "./savedMesh"
fileName = "mesh_27Avril2019"
description = "Variance pour l'axe des X = " + str(xSigma2) + " - " + "Variance pour l'axe des Y = " + str(ySigma2)
MeshMethods.saveMeshToFile(address, fileName, xLimits, yLimits, mesh, title = title, zTitle = zTitle, joinedDescription = description)