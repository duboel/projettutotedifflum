import numpy as np
import numpy.random as npr
import scipy as scp
import scipy.interpolate as scpi
import scipy.sparse as scps
from scipy.sparse import lil_matrix
from scipy.sparse import csr_matrix
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import DiscretizationMatrix
import Reshape
import LinearProblemVector
import PlotFunctions
import MeshMethods
import FourierGeneration

#################################
######## Grid parameters ########
#################################
xAxisPointsNumber = 2000
yAxisPointsNumber = 2000
xLimits = [-1.0, 1.0]
yLimits = [-1.0, 1.0]
mu = 0.0
sigma2 = 1.0
regularityConst = 1.5
curvesNumber = 50
tolerance = xAxisPointsNumber*yAxisPointsNumber*1e-15
#################################
#################################

############################
######## First Mesh ########
############################
Zh = npr.normal(mu, sigma2, (xAxisPointsNumber, yAxisPointsNumber))
Zs = FourierGeneration.genZs((xAxisPointsNumber, yAxisPointsNumber), mu, sigma2, regularityConst)
Zh, Zs, tol = FourierGeneration.iterate(Zh, Zs)
PlotFunctions.plotMesh2D(   xLimits,
                            yLimits,
                            Zh,
                            curvesNumber = curvesNumber)
PlotFunctions.plotMesh2D(   xLimits,
                            yLimits,
                            Zs,
                            curvesNumber = curvesNumber)
############################
############################

Zh, Zs, tol = FourierGeneration.iterate(Zh, Zs)
counter = 1
maxIter = 1000
while max(tol) > tolerance and counter < 1000 : 
    Zh, Zs, tol = FourierGeneration.iterate(Zh, Zs)
    if counter % 10 == 0 :
        print(counter, " - tol = ", max(tol))
    counter += 1


PlotFunctions.plotMesh2D(   xLimits,
                            yLimits,
                            Zh,
                            curvesNumber = curvesNumber)

plt.show()

PlotFunctions.plotMesh2D(   xLimits,
                            yLimits,
                            Zs,
                            curvesNumber = curvesNumber)

plt.show()

####################################
