import numpy as np
import numpy.random as npr
import scipy as scp
import scipy.interpolate as scpi
import scipy.sparse as scps
from scipy.sparse import lil_matrix
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def getDiscretizationMatrix2D(xLimits, yLimits, xAxisPointsNumber, yAxisPointsNumber) :

    matrix = lil_matrix((2*(xAxisPointsNumber - 2)*(yAxisPointsNumber - 2), (xAxisPointsNumber - 2)*(yAxisPointsNumber - 2)), dtype = np.float64)
    xDelta = (xLimits[1] - xLimits[0])/float(xAxisPointsNumber - 1)
    yDelta = (yLimits[1] - yLimits[0])/float(yAxisPointsNumber - 1)

    ####### Bottom Line ########################################################################################
    #### Left Side ####
    ## horizontal constraint
    matrix[0, 0] = - 1.0/xDelta
    matrix[0, 1] = 0.5/xDelta

    ## vertical constraint
    matrix[1, 0] = - 1.0/yDelta
    matrix[1, xAxisPointsNumber - 2] = 0.5/yDelta
    ###################

    #### Intermediate Columns ####
    for column in range(1, xAxisPointsNumber - 3) :
        ## horizontal constraints
        matrix[2*column, column] = - 1.0/xDelta
        matrix[2*column, column + 1] = 0.5/xDelta
        matrix[2*column, column - 1] = 0.5/xDelta

        ## vertical constraints
        matrix[2*column + 1, column] = - 1.0/yDelta
        matrix[2*column + 1, column + xAxisPointsNumber - 2] = 0.5/yDelta
    ##############################
    
    #### Right Side ####
    ## horizontal constraint
    matrix[2*xAxisPointsNumber - 6, xAxisPointsNumber - 3] = - 1.0/xDelta
    matrix[2*xAxisPointsNumber - 6, xAxisPointsNumber - 4] = 0.5/xDelta

    ## vertical constraint
    matrix[2*xAxisPointsNumber - 5, xAxisPointsNumber - 3] = - 1.0/yDelta
    matrix[2*xAxisPointsNumber - 5, 2*xAxisPointsNumber - 5] = 0.5/yDelta
    ###################
    ############################################################################################################

    ####### Intermediate Line ########################################################################################
    for line in range(1, yAxisPointsNumber - 3) : 

        #### Left Side ####
        ## horizontal constraint
        matrix[2*line*(xAxisPointsNumber - 2), line*(xAxisPointsNumber - 2)] = - 1.0/xDelta
        matrix[2*line*(xAxisPointsNumber - 2), line*(xAxisPointsNumber - 2) + 1] = 0.5/xDelta
        ## vertical constraint
        matrix[2*line*(xAxisPointsNumber - 2) + 1, line*(xAxisPointsNumber - 2)] = - 1.0/yDelta
        matrix[2*line*(xAxisPointsNumber - 2) + 1, (line + 1)*(xAxisPointsNumber - 2)] = 0.5/yDelta
        matrix[2*line*(xAxisPointsNumber - 2) + 1, (line - 1)*(xAxisPointsNumber - 2)] = 0.5/yDelta
        ###################

        for column in range(1, xAxisPointsNumber - 3) :

            #### Intermediate Columns ####
            ## horizontal constraints
            matrix[2*(line*(xAxisPointsNumber - 2) + column), line*(xAxisPointsNumber - 2) + column] = - 1.0/xDelta
            matrix[2*(line*(xAxisPointsNumber - 2) + column), line*(xAxisPointsNumber - 2) + column + 1] = 0.5/xDelta
            matrix[2*(line*(xAxisPointsNumber - 2) + column), line*(xAxisPointsNumber - 2) + column - 1] = 0.5/xDelta
            ## vertical constraints
            matrix[2*(line*(xAxisPointsNumber - 2) + column) + 1, line*(xAxisPointsNumber - 2) + column] = - 1.0/yDelta
            matrix[2*(line*(xAxisPointsNumber - 2) + column) + 1, (line + 1)*(xAxisPointsNumber - 2) + column] = 0.5/yDelta
            matrix[2*(line*(xAxisPointsNumber - 2) + column) + 1, (line - 1)*(xAxisPointsNumber - 2) + column] = 0.5/yDelta
            ###################

        #### Right Side ####
        ## horizontal constraint
        matrix[2*(line + 1)*(xAxisPointsNumber - 2) - 2, (line + 1)*(xAxisPointsNumber - 2) - 1] = - 1.0/xDelta
        matrix[2*(line + 1)*(xAxisPointsNumber - 2) - 2, (line + 1)*(xAxisPointsNumber - 2) - 2] = 0.5/xDelta 
        ## vertical constraint
        matrix[2*(line + 1)*(xAxisPointsNumber - 2) - 1, (line + 1)*(xAxisPointsNumber - 2) - 1] = - 1.0/yDelta
        matrix[2*(line + 1)*(xAxisPointsNumber - 2) - 1, (line + 2)*(xAxisPointsNumber - 2) - 1] = 0.5/yDelta
        matrix[2*(line + 1)*(xAxisPointsNumber - 2) - 1, line*(xAxisPointsNumber - 2) - 1] = 0.5/yDelta
        ###################
    ##################################################################################################################

    ####### Top Line ###################################################################################################################
    #### Left Side ####
    ## horizontal constraint
    matrix[2*(yAxisPointsNumber - 3)*(xAxisPointsNumber - 2), (yAxisPointsNumber - 3)*(xAxisPointsNumber - 2)] = - 1.0/xDelta
    matrix[2*(yAxisPointsNumber - 3)*(xAxisPointsNumber - 2), (yAxisPointsNumber - 3)*(xAxisPointsNumber - 2) + 1] = 0.5/xDelta
    ## vertical constraint
    matrix[2*(yAxisPointsNumber - 3)*(xAxisPointsNumber - 2) + 1, (yAxisPointsNumber - 3)*(xAxisPointsNumber - 2)] = - 1.0/yDelta
    matrix[2*(yAxisPointsNumber - 3)*(xAxisPointsNumber - 2) + 1, (yAxisPointsNumber - 4)*(xAxisPointsNumber - 2)] = 0.5/yDelta
    ###################

    #### Intermediate Columns ####
    for column in range(1, xAxisPointsNumber - 3) :
        ## horizontal constraints
        matrix[2*((yAxisPointsNumber - 3)*(xAxisPointsNumber - 2) + column), (yAxisPointsNumber - 3)*(xAxisPointsNumber - 2) + column] = - 1.0/xDelta
        matrix[2*((yAxisPointsNumber - 3)*(xAxisPointsNumber - 2) + column), (yAxisPointsNumber - 3)*(xAxisPointsNumber - 2) + column + 1] = 0.5/xDelta
        matrix[2*((yAxisPointsNumber - 3)*(xAxisPointsNumber - 2) + column), (yAxisPointsNumber - 3)*(xAxisPointsNumber - 2) + column - 1] = 0.5/xDelta

        ## vertical constraints
        matrix[2*((yAxisPointsNumber - 3)*(xAxisPointsNumber - 2) + column) + 1, (yAxisPointsNumber - 3)*(xAxisPointsNumber - 2) + column] = - 1.0/yDelta
        matrix[2*((yAxisPointsNumber - 3)*(xAxisPointsNumber - 2) + column) + 1, (yAxisPointsNumber - 4)*(xAxisPointsNumber - 2) + column] = 0.5/yDelta
    ##############################
    
    #### Right Side ####
    ## horizontal constraint
    matrix[2*(yAxisPointsNumber - 2)*(xAxisPointsNumber - 2) - 2, (yAxisPointsNumber - 2)*(xAxisPointsNumber - 2) - 1] = - 1.0/xDelta
    matrix[2*(yAxisPointsNumber - 2)*(xAxisPointsNumber - 2) - 2, (yAxisPointsNumber - 2)*(xAxisPointsNumber - 2) - 2] = 0.5/xDelta
    ## vertical constraint
    matrix[2*(yAxisPointsNumber - 2)*(xAxisPointsNumber - 2) - 1, (yAxisPointsNumber - 2)*(xAxisPointsNumber - 2) - 1] = - 1.0/yDelta
    matrix[2*(yAxisPointsNumber - 2)*(xAxisPointsNumber - 2) - 1, (yAxisPointsNumber - 3)*(xAxisPointsNumber - 2) - 1] = 0.5/yDelta
    ###################
    ####################################################################################################################################

    return matrix
