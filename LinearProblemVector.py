import numpy as np
import numpy.random as npr
import scipy as scp
import scipy.interpolate as scpi
import scipy.sparse as scps
from scipy.sparse import lil_matrix
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def getLinearProblemVector2D(xLimits, yLimits, topSide, bottomSide, leftSide, rightSide, xDifferentialFunction, yDifferentialFunction) :

    xAxisPointsNumber = len(topSide) - 2
    yAxisPointsNumber = len(leftSide)
    getIndex = lambda xIndex, yIndex : xIndex + yIndex*xAxisPointsNumber
    vector = np.zeros(2*xAxisPointsNumber*yAxisPointsNumber)
    xDelta = (xLimits[1] - xLimits[0])/float(xAxisPointsNumber + 1)
    yDelta = (yLimits[1] - yLimits[0])/float(yAxisPointsNumber + 1)

    for yIndex in range(yAxisPointsNumber) :
        for xIndex in range(xAxisPointsNumber) :
            vector[2*getIndex(xIndex, yIndex)] = xDifferentialFunction((xIndex + 1)*xDelta, (yIndex + 1)*yDelta)
            vector[2*getIndex(xIndex, yIndex) + 1] = yDifferentialFunction((xIndex + 1)*xDelta, (yIndex + 1)*yDelta)

    ### Top and bottom sides (vertical constraints)
    for xIndex in range(xAxisPointsNumber) : 
        vector[2*xIndex + 1] -= 0.5*bottomSide[xAxisPointsNumber - xIndex]/yDelta
        vector[2*getIndex(xIndex, yAxisPointsNumber - 1) + 1] -= 0.5*topSide[xAxisPointsNumber - xIndex]/yDelta

    ### Left and right sides (horizontal constraints)
    for yIndex in range(yAxisPointsNumber) : 
        vector[2*getIndex(0, yAxisPointsNumber - 1)] -= 0.5*leftSide[yAxisPointsNumber - yIndex - 1]/xDelta
        vector[2*getIndex(xAxisPointsNumber - 1, yIndex)] -= 0.5*rightSide[yAxisPointsNumber - yIndex - 1]/xDelta

    return vector

