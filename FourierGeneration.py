import numpy as np
import numpy.random as npr
import scipy as scp
import scipy.interpolate as scpi
import scipy.sparse as scps
from scipy.sparse import lil_matrix
from scipy.sparse import csr_matrix
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import copy as cp

import MeshMethods
import PlotFunctions

def extend(mesh) :

    shape = np.shape(mesh)
    meshExtension = np.zeros([2*shape[0] - 1, 2*shape[1] - 1])
    
    ######################
    #### middle cross ####
    ######################
    
    ### central value
    meshExtension[shape[0] - 1, shape[1] - 1] = mesh[0, 0]
    
    ## horizontal axis
    for columnIndex in range(shape[1] - 1) : 
        meshExtension[shape[0] - 1, shape[1] + columnIndex] = mesh[0, columnIndex + 1]
        meshExtension[shape[0] - 1, shape[1] - 2 - columnIndex] = mesh[0, columnIndex + 1]

    ## vertical axis
    for lineIndex in range(shape[0] - 1) : 
        meshExtension[shape[0] + lineIndex, shape[1] - 1] = mesh[lineIndex + 1, 0]
        meshExtension[shape[0] - 2 - lineIndex, shape[1] - 1] = mesh[lineIndex + 1, 0]
    
    ######################
    ######################
     
    ######################
    #### others zones ####
    ######################

    for lineIndex in range(shape[0] - 1) :
        for columnIndex in range(shape[1] - 1) :
            meshExtension[shape[0] + lineIndex, shape[1] + columnIndex] = mesh[lineIndex + 1, columnIndex + 1]
            meshExtension[shape[0] - 2 - lineIndex, shape[1] + columnIndex] = mesh[lineIndex + 1, columnIndex + 1]
            meshExtension[shape[0] + lineIndex, shape[1] - 2 - columnIndex] = mesh[lineIndex + 1, columnIndex + 1]
            meshExtension[shape[0] - 2 - lineIndex, shape[1] - 2 - columnIndex] = mesh[lineIndex + 1, columnIndex + 1]

    ######################
    ######################

    return meshExtension

def iterate(extendedZh, extendedZs) : 

    oldEZh = cp.copy(extendedZh)
    oldEZs = cp.copy(extendedZs)
    shape = np.shape(extendedZs)

    ## Phase I
    TZs = np.fft.fft2(extendedZs)
    TZh = np.fft.fft2(extendedZh)
    TZs = np.abs(TZs)/np.abs(TZh)*TZh
    extendedZs = np.real(np.fft.ifft2(TZs))
    extendedZh = np.real(np.fft.ifft2(TZh))

    ## Phase II
    shape = np.shape(extendedZs)
    Zs1D = np.reshape(extendedZs, (shape[0]*shape[1]))
    Zh1D = np.reshape(extendedZh, (shape[0]*shape[1]))
    argSortZs1D = np.argsort(Zs1D)
    argSortZh1D = np.argsort(Zh1D)
    copyZh1D = cp.copy(Zh1D)
    copyZs1D = cp.copy(Zs1D)
    for index in range(shape[0]*shape[1]) :
        Zh1D[argSortZh1D[index]] = copyZs1D[argSortZs1D[index]]
        Zs1D[argSortZs1D[index]] = copyZh1D[argSortZh1D[index]]
    extendedZh = np.reshape(Zh1D, shape)

    return extendedZh, extendedZs, abs(np.linalg.norm(extendedZs - extendedZh) - np.linalg.norm(oldEZs - oldEZh))

def genZs(shapeValues, mu, sigma2, regularityCoef) :

    Zs = np.zeros([shapeValues[0], shapeValues[1]])
    for i in range(shapeValues[0]) : 
        for j in range(shapeValues[1]) :
            Zs[i,j] = 1.0/(float((i+1)*(j+1)))**regularityCoef

    Zs[0,0] = 0.0
    Zs /= (np.linalg.norm(Zs)/np.sqrt(sigma2))
    Zs[0,0] = mu
    return np.abs(np.fft.ifft2(Zs))

def gen2DMesh(  xAxisPointsNumber,
                yAxisPointsNumber,
                xLimits, 
                yLimits,
                mu,
                sigma2,
                regularityCoef,
                thresholdPerPoint,
                maxIterationsNumber,
                address,
                filename,
                verboseFlag = True,
                iterationModulo = 20,
                plotFlag = True,
                curvesNumber = 50,
                meshTitle = "",
                xAxisTitle = "x",
                yAxisTitle = "y",
                zAxisTitle = "z",
                joinedDescription = "") :

    shape = (xAxisPointsNumber, yAxisPointsNumber)
    deltaX = abs(xLimits[1] - xLimits[0])
    deltaY = abs(yLimits[1] - yLimits[0])
    totalThreshold = 4*xAxisPointsNumber*yAxisPointsNumber*thresholdPerPoint
    extendedZs = extend(genZs(shape, mu, sigma2, regularityCoef))
    extendedZh = npr.normal(mu, sigma2, np.shape(extendedZs))
    counter = 1
    extendedZh, extendedZs, accuracyValue = iterate(extendedZh, extendedZs)

    while counter < maxIterationsNumber and accuracyValue > totalThreshold : 
        extendedZh, extendedZs, accuracyValue = iterate(extendedZh, extendedZs)
        if verboseFlag and counter % 20 == 0 :
            print("Iteration ", counter, " - Delta Accuracy = ", accuracyValue)
        counter += 1

    extendedZs = extendedZs[xAxisPointsNumber:, yAxisPointsNumber:]
    extendedZh = extendedZh[xAxisPointsNumber:, yAxisPointsNumber:]

    MeshMethods.saveMeshToFile( address,
                                filename,
                                xLimits,
                                yLimits,
                                extendedZs,
                                title = meshTitle,
                                xTitle = xAxisTitle,
                                yTitle = yAxisTitle,
                                zTitle = zAxisTitle,
                                joinedDescription = joinedDescription)
    
    if plotFlag :

        PlotFunctions.plotMesh2D(   xLimits,
                                    yLimits,
                                    extendedZs,
                                    curvesNumber = curvesNumber,
                                    title = meshTitle,
                                    xTitle = xAxisTitle,
                                    yTitle = yAxisTitle,
                                    zTitle = zAxisTitle)

        plt.show()

    return extendedZs, extendedZh
