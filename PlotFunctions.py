import numpy as np
import numpy.random as npr
import scipy as scp
import scipy.interpolate as scpi
import scipy.sparse as scps
from scipy.sparse import lil_matrix
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def plotMesh2D(xLimits, yLimits, mesh, curvesNumber = 50, title="", xTitle = "x", yTitle = "y", zTitle = "z") :

    xAxisPointsNumber, yAxisPointsNumber = mesh.shape
    xRange = np.linspace(xLimits[0], xLimits[1], xAxisPointsNumber)
    yRange = np.linspace(yLimits[0], yLimits[1], yAxisPointsNumber)
    X,Y = np.meshgrid(xRange, yRange)
    fig = plt.figure(figsize=(125, 100))
    ax = fig.add_subplot(111, projection="3d")

    ax.contour3D(X, Y, mesh, curvesNumber, cmap="binary")
    #ax.plot_surface(X, Y, mesh, cmap="viridis", edgecolor="none")
    ax.set_title(title)
    ax.set_xlabel(xTitle)
    ax.set_ylabel(yTitle)
    ax.set_zlabel(zTitle)


## Not functional
def plotMesh2Dv2(xLimits, yLimits, mesh, title="", xTitle = "x", yTitle = "y", zTitle = "z") :

    mesh = mesh.tolist()
    
    data = [    go.Surface( z = mesh,
                            contours =  go.surface.Contours(
                                        z = go.surface.contours.Z(
                                            show = True,
                                            usecolormap = True,
                                            highlightcolor = "#42f462",
                                            project = dict(z = True))))]
    
    layout = go.Layout( title = title,
                        autosize = False,
                        width = 500,
                        height = 500,
                        margin = dict(  l = 10,
                                        r = 10,
                                        b = 10,
                                        t = 10))
    
    ## fig = go.Figure(data = data, layout = layout)
    ## py.iplot(fig)
    py.iplot(data)

