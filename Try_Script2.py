import numpy as np
import numpy.random as npr
import scipy as scp
import scipy.interpolate as scpi
import scipy.sparse as scps
from scipy.sparse import lil_matrix
from scipy.sparse import csr_matrix
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


import DiscretizationMatrix
import Reshape
import LinearProblemVector
import PlotFunctions
import MeshMethods

address = "./savedMesh"
fileName = "mesh_18Avril2019"
curvesNumber = 100

datas = MeshMethods.readMeshFromFile(address, fileName)
PlotFunctions.plotMesh2D(   datas[2],
                            datas[3],
                            datas[4],
                            curvesNumber,
                            title=datas[5],
                            xTitle=datas[6],
                            yTitle=datas[7],
                            zTitle=datas[8])

plt.show()

